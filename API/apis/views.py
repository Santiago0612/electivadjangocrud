
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .models import Company
from django.http.response import JsonResponse
import json
# Create your views here.
class CompanyView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self,request,id=0):
        if (id>0):
            companies=list(Company.objects.filter(id=id).values())
            if len(companies)>0:
                company=companies[0]
                datos = {"message" : "Succes", "companies": company}
            else:
                datos = {"message" : "Companies not found..."}
            return JsonResponse(datos)  
        else:    
            companies=list(Company.objects.values())
            if len(companies)>0:
              datos={"message" : "Succes", "companies": companies}
            else:
                  datos={"message" : "Companies not found..."}
        return JsonResponse(datos)    



    def post(self,request):
        json_data=json.loads(request.body)
        Company.objects.create(name=json_data["name"], website=json_data["website"], foundation=json_data["foundation"])
        datos= {"message" : "Succes"}
        return JsonResponse(datos)
    
    def put(self,request,id):
        json_data= json.loads(request.body)
        companies = list(Company.objects.filter(id=id).values())
        if len(companies)>0:
            company= Company.objects.get(id=id)
            company.name=json_data["name"]
            company.website=json_data["website"]
            company.foundation=json_data["foundation"]
            company.save()
            datos= {"message" : "Succes"}
        else:
            datos={"message" : "Companies not found..."}
        return JsonResponse(datos)
        
    def delete(self,request,id):
        companies = list(Company.objects.filter(id=id).values())
        if len(companies)>0:
            Company.objects.filter(id=id).delete()
            datos= {"message" : "Succes"}
        else:    
            datos={"message" : "Companies not found..."}
        return JsonResponse(datos)    

        

